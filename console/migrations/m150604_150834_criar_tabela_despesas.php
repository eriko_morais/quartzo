<?php

use yii\db\Schema;
use yii\db\Migration;

class m150604_150834_criar_tabela_despesas extends Migration
{
    public function safeUp()
    {
            //categoria_depesas
        
            $this->createTable('categoria_despesa', [
                'id' => Schema::TYPE_PK,
                'categoria' => 'VARCHAR(100) NOT NULL'
                ]
            );
            
            $this->batchInsert('categoria_despesa',['categoria'],[
                ['Ordinárias'],
                ['Extraordinárias'],
                ['Individuais']
            ]);
            
            //modalidade
            
             $this->createTable('modalidade', [
                'id' => Schema::TYPE_PK,
                'nome' => 'VARCHAR(50) NOT NULL'
                ]
            );
            
            $this->batchInsert('modalidade',['nome'],[
                ['credito'],
                ['debito']
                
            ]);
            
            //titulo
            
            
             $this->createTable('titulo_despesa', [
                'id' => Schema::TYPE_PK,
                'titulo' => 'VARCHAR(50) NOT NULL'
                ]
            );
            
            $this->batchInsert('titulo_despesa',['titulo'],[
                ['luz'],
               
                              
            ]);
            
             //despesa
            $this->createTable('despesa', [
                'id' => Schema::TYPE_PK,
                'valor' => Schema::TYPE_DECIMAL . ' NOT NULL',
                'data' => Schema::TYPE_DATE . ' NOT NULL',
                'obs' => 'VARCHAR(200)',
                'id_sala' => Schema::TYPE_INTEGER . ' NOT NULL',
                'id_categoria' => Schema::TYPE_INTEGER . ' NOT NULL',
                'id_modalidade' => Schema::TYPE_INTEGER . ' NOT NULL',
                'id_titulo_despesa'=> Schema::TYPE_INTEGER . ' NOT NULL',
                'id_contrato' => Schema::TYPE_INTEGER. ' NOT NULL',
                'descricao' => 'VARCHAR(45)'
  
                ]
            );
            
            $this->addForeignKey('FK_despesa_sala', 'despesa', 'id_sala', 'sala', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_despesa_categoria', 'despesa', 'id_categoria', 'categoria_despesa', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_despesa_modalidade', 'despesa', 'id_modalidade', 'modalidade', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_despesa_titulo', 'despesa', 'id_titulo_despesa', 'titulo_despesa', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_despesa_contrato', 'despesa', 'id_contrato', 'contrato', 'id', 'RESTRICT','RESTRICT');
    }

    public function safeDown()
    {       $this->dropForeignKey('FK_despesa_contrato', 'despesa');
            $this->dropForeignKey('FK_despesa_titulo', 'despesa');
            $this->dropForeignKey('FK_despesa_modalidade', 'despesa');
            $this->dropForeignKey('FK_despesa_categoria', 'despesa');
            $this->dropForeignKey('FK_despesa_sala', 'despesa', 'id_sala', 'sala', 'id');
            
            
            $this->dropTable ('titulo_despesa');
            $this->dropTable('modalidade');
            $this->dropTable('categoria_despesa');

        
    }
    
   
}
