<?php

use yii\db\Schema;
use yii\db\Migration;

class m150604_130552_criar_tabela_sala extends Migration
{
    public function up()
    {
        $this->createTable('sala', [
            'id' => Schema::TYPE_PK,
            'numero' => 'VARCHAR(6) NOT NULL',
            'dimensao' => Schema::TYPE_INTEGER . ' NOT NULL',
            'finalidade' => 'VARCHAR(20) NOT NULL',
            'endereco' => 'VARCHAR(150) NOT NULL',
            'obs' => 'VARCHAR(150)'
        ]);
    }

    public function down()
    {
       $this->dropTable('sala');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
