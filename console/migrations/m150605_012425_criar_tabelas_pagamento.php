<?php

use yii\db\Schema;
use yii\db\Migration;

class m150605_012425_criar_tabelas_pagamento extends Migration
{
    public function safeUp()
    {
        //montante
         $this->createTable('montante', [
                'id' => Schema::TYPE_PK,
                'mes' => Schema::TYPE_INTEGER. ' NOT NULL',
                'ano' => Schema::TYPE_INTEGER. ' NOT NULL',
                'valor' => Schema::TYPE_DECIMAL . ' NOT NULL',
                'data' => Schema::TYPE_DATE . ' NOT NULL',
                'id_contrato' => Schema::TYPE_INTEGER. ' NOT NULL',
                'status' => Schema::TYPE_BOOLEAN . ' NOT NULL',
  
                ]
            );
        
        //pagamento
         $this->createTable('pagamento', [
                'id' => Schema::TYPE_PK,
                'valor' => Schema::TYPE_DECIMAL . ' NOT NULL',
                'acrescimo' => Schema::TYPE_DECIMAL . ' NOT NULL',
                'data' => Schema::TYPE_DATE . ' NOT NULL',
                'id_inquilino' => Schema::TYPE_INTEGER . ' NOT NULL',
                'id_montante'=> Schema::TYPE_INTEGER . ' NOT NULL',
                'id_contrato' => Schema::TYPE_INTEGER. ' NOT NULL',
                 
                ]);
            $this->addForeignKey('FK_pagamento_montante', 'pagamento', 'id_montante', 'montante', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_pagamento_inquilino', 'pagamento', 'id_inquilino', 'inquilino', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_pagamento_contrato', 'pagamento', 'id_contrato', 'contrato', 'id', 'RESTRICT','RESTRICT');
            
            
        
        //pagamento_aluguel
            $this->createTable('pagamento_aluguel', [
                'id' => Schema::TYPE_PK,
                'valor' => Schema::TYPE_DECIMAL . ' NOT NULL',
                'acrescimo' => Schema::TYPE_DECIMAL . ' NOT NULL',
                'data' => Schema::TYPE_DATE . ' NOT NULL',
                'id_inquilino' => Schema::TYPE_INTEGER . ' NOT NULL',
                'id_despesa'=> Schema::TYPE_INTEGER . ' NOT NULL',
                'id_contrato' => Schema::TYPE_INTEGER. ' NOT NULL',
                 
                ]);
            
            $this->addForeignKey('FK_pagamento_aluguel_despesa', 'pagamento_aluguel', 'id_despesa', 'despesa', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_pagamento_aluguel_inquilino', 'pagamento_aluguel', 'id_inquilino', 'inquilino', 'id', 'RESTRICT','RESTRICT');
            $this->addForeignKey('FK_pagamento_aluguel_contrato', 'pagamento_aluguel', 'id_contrato', 'contrato', 'id', 'RESTRICT','RESTRICT');
          
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_pagamento_aluguel_contrato', 'pagamento_aluguel');
        $this->dropForeignKey('FK_pagamento_aluguel_inquilino', 'pagamento_aluguel');
        $this->dropForeignKey('FK_pagamento_aluguel_despesa', 'pagamento_aluguel');
        
         $this->dropForeignKey('FK_pagamento_contrato', 'pagamento_aluguel');
        $this->dropForeignKey('FK_pagamento_inquilino', 'pagamento_aluguel');
        $this->dropForeignKey('FK_pagamento_montante', 'pagamento_aluguel');
       
        $this->dropTable('pagamento_aluguel');
       
               
        $this->dropTable('pagamento');
        
        $this->dropTable('montante');
        
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
