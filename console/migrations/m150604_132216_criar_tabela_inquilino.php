<?php

use yii\db\Schema;
use yii\db\Migration;

class m150604_132216_criar_tabela_inquilino extends Migration
{
    public function safeUp()
    {
        
             $this->createTable('inquilino', [
                'id' => Schema::TYPE_PK,
                'nome' => 'VARCHAR(100) NOT NULL',
                'nacionalidade' => 'VARCHAR(50) NOT NULL',
                'estado_civil' => 'VARCHAR(50) NOT NULL',
                'profissao' => 'VARCHAR(100) NOT NULL',
                'identidade' => Schema::TYPE_INTEGER . ' NOT NULL',
                'cpf' => Schema::TYPE_INTEGER . ' NOT NULL',
                'endereco' => 'VARCHAR(150) NOT NULL',
                'bairro' => 'VARCHAR(100) NOT NULL',
                'fone' => 'VARCHAR(12) NOT NULL',
                'email' => 'VARCHAR(150)',
                'obs' => 'VARCHAR(150)'
        ]);
             $this->addForeignKey('fk_inquilino_user', 'inquilino', 'id', 'user', 'id', 'RESTRICT','RESTRICT');
                        
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_inquilino_user', 'inquilino');
        $this->dropTable('inquilino');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
