<?php

use yii\db\Schema;
use yii\db\Migration;

class m150604_143425_criar_tabela_contrato extends Migration
{
    public function safeUp()
    {
        $this->createTable('contrato', [
            'id' => Schema::TYPE_PK,
            'data_inicio' => Schema::TYPE_DATE,
            'data_termino' => Schema::TYPE_DATE,
            'id_sala' => Schema::TYPE_INTEGER . ' NOT NULL',
            'ativo' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'dia_vencimento' => Schema::TYPE_SMALLINT . ' NOT NULL',
    
        ]);
        
        $this->addForeignKey('fk_contrato_sala', 'contrato', 'id_sala', 'sala', 'id');
        
        $this->createTable('contrato_inquilino', [
            'id_contrato' => Schema::TYPE_INTEGER,
            'id_inquilino' => Schema::TYPE_INTEGER,
            'percentual_pagamento' => Schema::TYPE_SMALLINT,
            
        ]);
        
        $this->addPrimaryKey ('PK_contrato_inquilino','contrato_inquilino', [
            'id_contrato','id_inquilino']);
        
        $this->addForeignKey('fk_contrato_inquilino_contrato', 'contrato_inquilino', 'id_contrato', 'contrato', 'id');
        $this->addForeignKey('fk_contrato_inquilino_inquinlino', 'contrato_inquilino', 'id_inquilino', 'inquilino', 'id');
        
    }

    public function safeDown()
    {
        
      
        
         $this->dropForeignKey ('fk_contrato_inquilino_contrato','contrato_inquilino');
         $this->dropForeignKey('fk_contrato_inquilino_inquinlino', 'contrato_inquilino');
         
         $this->dropForeignKey('fk_contrato_sala', 'contrato');
           
           
          $this->dropTable('contrato_inquilino');
          $this->dropTable('contrato');
          
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
