<?php

use yii\db\Schema;
use yii\db\Migration;

class m150604_142303_criar_tabela_funcionario extends Migration {

    public function safeUp()
    {

        $this->createTable('funcionario', [
            'id' => Schema::TYPE_PK,
            'nome' => 'VARCHAR(150) NOT NULL',
            'endereco' => 'VARCHAR(150) NOT NULL',
            'estado_civil' => 'VARCHAR(8) NOT NULL',
            'n_filhos_menores' => Schema::TYPE_INTEGER . ' NOT NULL',
            'rg' => Schema::TYPE_INTEGER . ' NOT NULL',
            'cpf' => Schema::TYPE_INTEGER . ' NOT NULL',
            'ctps' => Schema::TYPE_INTEGER . ' NOT NULL',
            'pis_pasep' => Schema::TYPE_INTEGER . ' NOT NULL',
            'funcao' => 'VARCHAR(200) NOT NULL',
            'data_admissao' => Schema::TYPE_DATE,
            'data_demissao' => Schema::TYPE_DATE,
            'salario' => Schema::TYPE_DECIMAL,
            'carga_horaria' => Schema::TYPE_SMALLINT . ' NOT NULL',
            'email' => 'VARCHAR(150)' . ' NOT NULL',
            'obs' => 'VARCHAR(200)'
        ]);

        $this->addForeignKey('fk_funcionario_user', 'funcionario', 'id', 'user', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_funcionario_user', 'funcionario');
        $this->dropTable('funcionario');
    }

}
