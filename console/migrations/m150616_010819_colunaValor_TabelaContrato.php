<?php

use yii\db\Schema;
use yii\db\Migration;

class m150616_010819_colunaValor_TabelaContrato extends Migration
{
    public function SafeUp()
    {
            $this->addColumn('Contrato', 'valor', Schema::TYPE_DECIMAL);
    }

    public function SafeDown()
    {
         $this->removeColumn('Contrato', 'valor', Schema::TYPE_DECIMAL);

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
